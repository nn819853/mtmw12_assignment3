# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:54:32 2019

@author: nn819853
"""

import numpy as np

# Functions for calculating gradients numerically

def gradient_2point(f,dx):
    """
    Calculates the gradient of array f assuming points are a distance
    dx apart using 2-point finite differences
    : input: f: array of values of function f
    : input: dx: distance in x between points in f
    : returns: array of gradients of f with respect to x, with the same 
    length as array f
    """
    
    # initialise output gradient array to be same size as input f array
    dfdx = np.zeros_like(f)
    
    # uncentred backward and forward two point differences at the end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    
    # centred 2-point differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx


def gradient_4point(f,dx):
    """
    Calculates the gradient of array f assuming points are a distance
    dx apart using 4-point finite differences
    : input: f: array of values of function f
    : input: dx: distance in x between points in f
    : returns: array of gradients of f with respect to x, with the same 
    length as array f
    """
    
    # initialise output gradient array to be same size as input f array
    dfdx = np.zeros_like(f)
    
    # uncentred backward and forward two point differences at the end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    
    # centred 2-point differences at the second and second-last points:
    dfdx[1] = (f[2] - f[0])/(2*dx)
    dfdx[-2] = (f[-1] - f[-3])/(2*dx)
    
    # centred 4-point differences for the mid-points
    for i in range(2,len(f)-2):
        dfdx[i] = (f[i-2] - 8*f[i-1] + 8*f[i+1] - f[i+2])/(12*dx)
    return dfdx

