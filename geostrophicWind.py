# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 15:12:49 2019

@author: nn819853
"""

# MTMW12 assignment 3. Max Coleman 15/10/2019
# Code to calculate and illustrate geostrophic wind using 2-point numerical 
# differentiation and the error of the approximation.

import numpy as np
import matplotlib.pyplot as plt
import geoParameters as gp
from differentiate import *

def geostrophicWind():
    
    # Determine dy from resolution
    N = 10  # the number of intervals 
    dy = (gp.ymax - gp.ymin)/N  # length of spacing in y
    
    # Create array of y at defined spacing
    y = np.linspace(gp.ymin,gp.ymax,N+1)
    
    # Geostrophic wind calculated from the analytically determined gradient
    uExact = gp.uExact(y)
    
    # Pressure determined at the y points
    p = gp.pressure(y)
    
    # Pressure gradient and geostrophic wind calculated from the numerical
    # 2-point differences
    dpdy = gradient_2point(p,dy)
    u_2point = gp.geoWind(dpdy)
    
    # Graph to compare numerical and analytical solutions
    # plot using large fonts
    font = {'size':14}
    plt.rc('font',**font)
    
    # Plot the approximate and exact geostrophic wind at y points
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label='Two-point differences', \
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWind_2point_N10.png')
    plt.show()
    
    # Determine error between analytical and numerical solutions
    # Initialise error array
    e = np.zeros_like(y)
    
    # Determine numerical approximation minus exact geostrophic wind
    for i in range(0,len(y)):
        e[i] = u_2point[i] - uExact[i]
    
    # Plot graph of log (base 10) of errors at y points
    plt.plot(y/1000, np.log10(np.abs(e)), 'xk', label='Error', ms=10)
    plt.xlabel('y (km)')
    plt.ylabel('log |u error (m/s)|')
    plt.tight_layout()
    plt.savefig('geoWind_2point_N10_error.png')
    plt.show()
    
if __name__ == "__main__":
    geostrophicWind()
    