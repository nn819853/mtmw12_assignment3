# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:21:05 2019

@author: nn819853
"""

import numpy as np

# Parameters for calculating geostrophic wind

pa = 1e5  # mean pressure / Pa
pb = 200  # magnitude of pressure variations / Pa
f = 1e-4  # Coriolis parameter / s**-1
rho = 1  # density / kg.m**-3
L = 2.4e6  # length scale of pressure variations / m
ymin = 0  # maximum space dimension / m
ymax = 1e6  # minimum space dimension


def pressure(y):
    """
    Calculates pressure at given y locations
    : input: y: y location in m
    : returns: pressure in Pa
    """
    
    return pa + pb*np.cos(y*np.pi/L)


def uExact(y):
    """
    Calculates the analytic geostrophic wind at given y locations
    : input: y: y location in m
    : returns: analytic geostrophic wind in m.s**-1
    """
    
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)
    
def geoWind(dpdy):
    """
    Calculates the geostrophic wind at given y locations
    : input: dpdy: pressure gradient in y in Pa*m**-1
    : returns: geostrophic wind in m*s**-1
    """
    
    return -dpdy/(rho*f)

