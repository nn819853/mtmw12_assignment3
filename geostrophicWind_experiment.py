# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 15:12:49 2019

@author: nn819853
"""

# MTMW12 assignment 3. Max Coleman 15/10/2019
# Code to calculate geostrophic wind using 2-point numerical differentiation 
# for different interval numbers and determine and illustrate the error of the 
# approximations and their order of accuracy

import numpy as np
import matplotlib.pyplot as plt
import geoParameters as gp
from differentiate import *

def geostrophicWind_experiment():
    
    # Set different interval numbers 
    N = [10,20,100]  
    
    # Determine the smallest number of intervals and create array of y
    # distances of length equal to the smallest number of intervals
    min_N = np.min(N)
    y_min_N = np.linspace(gp.ymin,gp.ymax,min_N+1)
    
    # Initialise arrays for approximated u and error for each y and N.
    u_2point_N = np.full((len(N),min_N+1),np.NaN)
    error_N = np.full((len(N),min_N+1),np.NaN)
    
    # Initialise array for length of y spacing, dy, for each N
    dy_N = np.zeros_like(N) 
    
    # Geostrophic wind calculated from the analytically determined gradient
    uExact = gp.uExact(y_min_N)
    
    # Calculate u for each y and N
    for n in range(0,len(N)):
        dy_N[n] = (gp.ymax - gp.ymin)/N[n]  # length of spacing in y
        
        # Create array of y at defined spacing
        y = np.linspace(gp.ymin,gp.ymax,N[n]+1)
        
        # Pressure determined at the y points
        p = gp.pressure(y)
        
        # Pressure gradient and geostrophic wind calculated from the numerical
        # 2-point differences
        dpdy = gradient_2point(p,dy_N[n])
        u_2point = gp.geoWind(dpdy)
        u_2point_N[n,:] = u_2point[range(0,N[n]+1,int(N[n]/min_N))]
                
        # Determine numerical approximation minus exact geostrophic wind
        for i in range(0,len(y_min_N)):
            error_N[n,i] = u_2point_N[n,i] - uExact[i]
                
    # plot using large fonts
    font = {'size':14}
    plt.rc('font',**font)
    
    # Plot graph of errors at y points
    plt.plot(y_min_N/1000, np.log10(np.abs(error_N[0])), 'xk', \
             label='N = 10', ms=10)
    plt.plot(y_min_N/1000, np.log10(np.abs(error_N[1])), 'xb', \
             label='N = 20', ms=10)
    plt.plot(y_min_N/1000, np.log10(np.abs(error_N[2])), 'xr', \
             label='N = 100', ms=10)
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('log |u error (m/s)|')
    plt.tight_layout()
    plt.savefig('geoWind_2point_N10-20-100_errors.png')
    plt.show()
    
    # Initialise accuracy order array    
    order_error = np.zeros_like(y_min_N)
    
    # Calculate order of accuracy for each y
    for i in range(0,len(y_min_N)):
        order_error[i] = np.log10(error_N[0,i]/error_N[2,i])\
        /np.log10(dy_N[0]/dy_N[2])
    
    # Plot graph of order of accuracy with y
    plt.plot(y_min_N/1000, order_error, 'xk', ms=10)
    plt.xlabel('y (km)')
    plt.ylabel('order of error, n')
    plt.tight_layout()
    plt.savefig('geoWind_2point_N10-20-100_error_orders.png')
    plt.show()
            
    
if __name__ == "__main__":
    geostrophicWind_experiment()